package com.curse.ejercicio;

@FunctionalInterface
public interface InterfaceWithoutParameters {
	public abstract NPC randomize();
}
