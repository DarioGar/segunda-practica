package com.curse.ejercicio;

@FunctionalInterface
public interface InterfaceWithOneParameter {

	public abstract boolean test(House h1);
}
