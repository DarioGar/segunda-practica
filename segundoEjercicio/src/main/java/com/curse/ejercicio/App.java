package com.curse.ejercicio;

import java.awt.List;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;


public class App {

	public static void main(String[] args) {
		
		String separator = "\n=============================================\n";

		Random r = new Random();
		
		NPC npc1 = new NPC(1,"Brandis",Optional.of("La hermandad del acero"),1D,1D,true,Optional.empty());
		NPC npc2 = new NPC(2,"Vargas",Optional.of("La hermandad del acero"),2D,1D,false,Optional.empty());
		NPC npc3 = new NPC(3,"John Maxson",Optional.of("La hermandad del acero"),1D,2D,true,Optional.empty());
		
		NPC npc4 = new NPC(4,"Henry",Optional.of("El enclave"),1D,1D,true,Optional.empty());
		NPC npc5 = new NPC(5,"Orion Moreno",Optional.of("El enclave"),2D,1D,true,Optional.empty());
		NPC npc6 = new NPC(6,"Judah",Optional.of("El enclave"),1D,3D,true,Optional.empty());
		
		NPC npc7 = new NPC(7,"Roberto",Optional.empty(),2D,1D,false,Optional.empty());
		NPC npc8 = new NPC(8,"Auld Lang",Optional.empty(),3D,1D,false,Optional.empty());
		NPC npc9 = new NPC(9,"Arcade Gannon",Optional.empty(),4D,1D,false,Optional.empty());
		
		House h1 = new House(1,"casa1",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc7),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h2 = new House(2,"casa2",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc1),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h3 = new House(3,"casa3",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc2),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h4 = new House(4,"casa4",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc3),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h5 = new House(5,"casa5",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc4),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h6 = new House(6,"casa6",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc5),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h7 = new House(7,"casa7",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc7),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h8 = new House(8,"casa8",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc6),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h9 = new House(9,"casa9",r.nextInt(10 - 1 + 1) + 1,Optional.of(npc7),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h10 = new House(10,"casa10",r.nextInt(10 - 1 + 1) + 1,Optional.empty(),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h11 = new House(11,"casa11",r.nextInt(10 - 1 + 1) + 1,Optional.empty(),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		House h12 = new House(12,"casa12",r.nextInt(10 - 1 + 1) + 1,Optional.empty(),r.nextDouble()*10,r.nextDouble()*10,Optional.empty());
		
		ArrayList<House> array1 = new ArrayList();
		
		array1.add(h1);
		array1.add(h2);
		array1.add(h3);
		array1.add(h4);
		array1.add(h5);
		array1.add(h6);
		
		array1.add(h7);
		array1.add(h8);
		array1.add(h9);
		array1.add(h10);
		array1.add(h11);
		array1.add(h12);

		mostrar(array1);
		System.out.println(separator);
		obtenerDuenioDeCasa(h5);
		System.out.println(separator);
		obtenerFactionOfOwner(h1);
		System.out.println(separator);
		moreThanFiveRooms(h4);
		System.out.println(separator);
		generateRandomNPC();
		System.out.println(separator);
		obtenerCasasDe(7, array1);
		System.out.println(separator);
		visitNPCInTheFuture(npc1);
		System.out.println(separator);
		//differenceBetweenLastVisitedAndNow(npc1);
	}
	
	//Streams
	
	public static void mostrar(ArrayList<House> array1) {
		Stream<House> houseStream = array1.stream();
		houseStream.forEach(project -> System.out.println(project));
	}
	
	public static void obtenerCasasDe(Integer id,ArrayList<House> array1) {
		Stream<House> houseStream = array1.stream();
		houseStream.filter(house -> house.getOwner().isPresent()).filter(house -> house.getOwner().get().getId() == id).forEach(o -> System.out.println(o));
	}
	
	//Optionals
	
	public static void obtenerDuenioDeCasa(House h) {
		Optional<NPC> owner = h.getOwner();
		System.out.println(owner.isPresent() ? "The owner of house " + h.getName() + " is " + owner.get().getName() : "House " + h + " does not have an owner");
	}
	
	public static void obtenerFactionOfOwner(House h) {
		Optional<NPC> owner = h.getOwner();
		if(owner.isPresent()) {
			Optional<String> faction = owner.get().getFaction();
			System.out.println(faction.isPresent() ? owner.get().getName() + " belongs to: " + faction.get() : owner.get().getName() + " doesnt have a faction");
		}
	}
	
	//Lambdas
	
	public static void moreThanFiveRooms(House h) {
		InterfaceWithOneParameter iwop = new InterfaceWithOneParameter() {
			
			public boolean test(House h1) {
				return h1.getRooms() >=5;
			}
		};
		
		System.out.println(iwop.test(h) ? h + "tiene al menos 5 habitaciones" : h + "tiene menos de 5 habitaciones");
	}
	
	public static void generateRandomNPC() {
		Random r = new Random();
		InterfaceWithoutParameters iwp = () ->  new NPC(r.nextInt());
		
		System.out.println(iwp.randomize());
	}
	//Times

	public static void visitNPCInTheFuture(NPC npc) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy | hh:mm:ss");
		LocalDateTime ldt = LocalDateTime.now();
		npc.setLastInteraction(Optional.of(ldt.plusYears(30).format(dtf).toString()));
		System.out.println("Changed last visited: " + npc);
	}
	
	//Se me acabó el tiempo sin hacer que funcione esto :(
	
//	public static void differenceBetweenLastVisitedAndNow(NPC npc) {
//		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy | hh:mm:ss");
//
//		LocalDateTime ld1 = LocalDateTime.parse(npc.getLastInteraction().get(), dtf);
//		LocalDateTime ld2 = LocalDateTime.now();
//		Period p = Period.between(ld1.toLocalDate(), ld2.toLocalDate());
//		System.out.println("Días hasta la visita: " + p);
//	}
}
