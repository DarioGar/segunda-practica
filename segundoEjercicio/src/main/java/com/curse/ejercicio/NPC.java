package com.curse.ejercicio;

import java.util.Optional;

public class NPC {
	
	private Integer id;
	private String name;
	private Optional<String> faction;
	private Double xCoord;
	private double yCoord;
	private boolean interactive;
	private Optional<String> lastInteraction;
	
	public NPC(Integer id) {
		this.id = id;
	}
	
	public NPC(Integer id, String name, Optional<String> faction, Double xCoord, double yCoord, boolean interactive,Optional<String> lastInteraction) {
		super();
		this.id = id;
		this.name = name;
		this.faction = faction;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.interactive = interactive;
		this.lastInteraction = lastInteraction;
	}

	@Override
	public String toString() {
		return "NPC [id=" + id + ", name=" + name + ", faction=" + faction + ", xCoord=" + xCoord + ", yCoord=" + yCoord
				+ ", interactive=" + interactive + ", lastInteraction=" + lastInteraction + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Optional<String> getFaction() {
		return faction;
	}

	public void setFaction(Optional<String> faction) {
		this.faction = faction;
	}

	public Double getxCoord() {
		return xCoord;
	}

	public void setxCoord(Double xCoord) {
		this.xCoord = xCoord;
	}

	public double getyCoord() {
		return yCoord;
	}

	public void setyCoord(double yCoord) {
		this.yCoord = yCoord;
	}

	public boolean isInteractive() {
		return interactive;
	}

	public void setInteractive(boolean interactive) {
		this.interactive = interactive;
	}

	public Optional<String> getLastInteraction() {
		return lastInteraction;
	}

	public void setLastInteraction(Optional<String> lastInteraction) {
		this.lastInteraction = lastInteraction;
	}
	
	

}
