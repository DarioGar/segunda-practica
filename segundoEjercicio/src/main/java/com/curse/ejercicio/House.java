package com.curse.ejercicio;

import java.util.Optional;

public class House {

	private Integer id;
	private String name;
	private Integer rooms;
	private Optional<NPC> owner;
	private Double xCoord;
	private double yCoord;
	private Optional<String> lastVisited;
	
	
	
	@Override
	public String toString() {
		return "House [id=" + id + ", name=" + name + ", rooms=" + rooms + ", owner=" + owner + ", xCoord=" + xCoord
				+ ", yCoord=" + yCoord + ", lastVisited=" + lastVisited + "]";
	}

	public House(Integer id, String name, Integer rooms, Optional<NPC> owner, Double xCoord, double yCoord,
			Optional<String> lastVisited) {
		super();
		this.id = id;
		this.name = name;
		this.rooms = rooms;
		this.owner = owner;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.lastVisited = lastVisited;
	}

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}

	public Optional<NPC> getOwner() {
		return owner;
	}

	public void setOwner(Optional<NPC> owner) {
		this.owner = owner;
	}

	public Double getxCoord() {
		return xCoord;
	}

	public void setxCoord(Double xCoord) {
		this.xCoord = xCoord;
	}

	public double getyCoord() {
		return yCoord;
	}

	public void setyCoord(double yCoord) {
		this.yCoord = yCoord;
	}

	public Optional<String> getLastVisited() {
		return lastVisited;
	}

	public void setLastVisited(Optional<String> lastVisited) {
		this.lastVisited = lastVisited;
	}

	
	
}